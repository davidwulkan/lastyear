import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

title:string = 'Welcome';
userEmail:string;
user:Observable<User | null>;

constructor(public auth:AuthService) { }

userId:string;

getUser(){
  this.auth.getUser();
}

ngOnInit(): void {
  this.auth.getUser().subscribe(
    user => {
      this.userId = user.uid;
      console.log(this.userId);

      this.userEmail=user.email;
    }
  );
}

}
