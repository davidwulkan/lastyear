import { Comments } from './interfaces/comments';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  post:string;
  private POSTURL="https://jsonplaceholder.typicode.com/posts"
  private commentsURL="https://jsonplaceholder.typicode.com/comments/"

constructor(private http:HttpClient, private db:AngularFirestore) { }

    getPosts():Observable<Posts[]>{
      console.log("in getPosts ")
    return this.http.get<Posts[]>(`${this.POSTURL}`);
    }

   getComments():Observable<Comments[]>{
    console.log("in getComments ")
    return this.http.get<Comments[]>(`${this.commentsURL}`);

  }

  postCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  addSaved(userId:string,title:string){
    const post = {title:title}; 
    this.userCollection.doc(userId).collection('posts').add(post);
    console.log('post:', post)
  }

     public updateLikes(userId:string, id:string, likes:number){
      this.db.doc(`users/${userId}/posts/${id}`).update(
        {
         likes:likes
        
        }
      )
  
    }

  
  
}
