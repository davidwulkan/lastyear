// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAhV2FbZ7xiTYwVeI0RPPJ318UsU30b8K0",
    authDomain: "lastyear-2020.firebaseapp.com",
    projectId: "lastyear-2020",
    storageBucket: "lastyear-2020.appspot.com",
    messagingSenderId: "433539927194",
    appId: "1:433539927194:web:a295481d6a25e9d645e323"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
